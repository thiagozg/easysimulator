### EasySimulator Challenge
* 100% Kotlin
* Testes unitários e instrumentados
* RxJava e Retrofit para comunicação com a API
* Dagger para a injeção de dependências
* ConstraintLayout para a construção dos layouts XMLs
* Padrão de Arquitetura MVVM
* Architecture Components para camadas da arquitetura