package br.com.thiagozg.easysimulator.features.form

import android.content.Intent
import android.os.Parcelable
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra
import android.support.test.espresso.intent.matcher.IntentMatchers.toPackage
import android.support.test.rule.ActivityTestRule
import br.com.concretesolutions.kappuccino.extensions.type
import br.com.thiagozg.easysimulator.R
import br.com.thiagozg.easysimulator.features.result.ResultActivity.Companion.KEY_INPUT_FORM
import br.com.thiagozg.easysimulator.viewBy
import org.hamcrest.CoreMatchers.allOf

class FormActivityArrangeRobot {
    fun startFormActivity(rule: ActivityTestRule<FormActivity>) = rule.launchActivity(Intent())
}

class FormActivityActRobot {
    fun typeInputAmountDatePercent() {
        viewBy(R.id.tieAmountValue).type("100050")
        viewBy(R.id.tieDate).type("10102025")
        viewBy(R.id.tiePercent).type("125")
    }
}

class FormActivityAssertRobot {
    fun checkIntentData() {
        intending(allOf(
            toPackage("br.com.thiagozg.easysimulator.features.result.ResultActivity"),
            hasExtra(KEY_INPUT_FORM, Parcelable::class)
        ))
    }
}
