package br.com.thiagozg.easysimulator.features.result

import android.content.Intent
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.rule.ActivityTestRule
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import br.com.concretesolutions.kappuccino.assertions.VisibilityAssertions.displayed
import br.com.thiagozg.easysimulator.R
import br.com.thiagozg.easysimulator.R.id.*
import br.com.thiagozg.easysimulator.features.formatToMoney
import br.com.thiagozg.easysimulator.features.formatToPercent
import br.com.thiagozg.easysimulator.features.result.ResultActivity.Companion.KEY_INPUT_FORM
import br.com.thiagozg.easysimulator.features.textValue
import br.com.thiagozg.easysimulator.model.domain.FormDTO
import br.com.thiagozg.easysimulator.readJsonFile
import kotlinx.android.synthetic.main.activity_form.*
import kotlinx.android.synthetic.main.activity_result.*
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

class ResultActivityArrangeRobot {
    fun mockSimulatorRequest(server: MockWebServer, fileName: String) =
        server.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(readJsonFile(getInstrumentation().context, fileName))
        )

    fun startResultActivity(rule: ActivityTestRule<ResultActivity>) = rule.launchActivity(Intent().apply {
        putExtra(KEY_INPUT_FORM, FormDTO(
            investedAmount = "R$1.000,00",
            maturityDate = "15/10/2025",
            rate = "120%"
        ))
    })
}

class ResultActivityAssertRobot {
    fun checkDataIsShowing() {
        displayed {
            allOf {
                id(R.id.tvTotalValue)
                text("R$2.501,28")
            }
            id(R.id.labelTotalIncome).text("Rendimento total de R$1.501,28")
            id(R.id.tvValueInitial).text("R$1.000,00")
            allOf {
                id(R.id.tvValueRaw)
                text("R$2.501,28")
            }
            id(R.id.tvIncomeValue).text("R$1.501,28")
            id(R.id.tvItInvestment).text("R$225,19 (15.00%)")
            id(R.id.tvInvestmentNetValue).text("R$2.276,09")
            id(R.id.tvRescueDate).text("15/10/2025")
            id(R.id.tvRunningDays).text("2557")
            id(R.id.tvMonthlyIncome).text("0.88%")
            id(R.id.tvCdiPercentage).text("120%")
            id(R.id.tvAnnualProfitability).text("150.13%")
            id(R.id.tvPeriodProfitability).text("11.15%")
        }
    }
}
