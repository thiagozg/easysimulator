package br.com.thiagozg.easysimulator.features.form

import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.runner.AndroidJUnit4
import br.com.thiagozg.easysimulator.features.BaseActivityTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FormActivityTest : BaseActivityTest() {

    @get:Rule val activityRule = IntentsTestRule(FormActivity::class.java, false, false)

    @Test
    fun checkIntentForResultActivity() {
        arrange {
            startFormActivity(activityRule)
        }
        act {
            typeInputAmountDatePercent()
        }
        assert {
            checkIntentData()
        }
    }

    private fun arrange(func: FormActivityArrangeRobot.() -> Unit) = FormActivityArrangeRobot().apply { func() }

    private fun act(func: FormActivityActRobot.() -> Unit) = FormActivityActRobot().apply { func() }

    private fun assert(func: FormActivityAssertRobot.() -> Unit) = FormActivityAssertRobot().apply { func() }

}