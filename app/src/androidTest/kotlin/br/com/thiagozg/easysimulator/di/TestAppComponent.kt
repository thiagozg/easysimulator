package br.com.thiagozg.easysimulator.di

import br.com.thiagozg.easysimulator.TestApplication
import br.com.thiagozg.easysimulator.di.modules.BuildersModule
import br.com.thiagozg.easysimulator.di.modules.TestAndroidModule
import br.com.thiagozg.easysimulator.di.modules.TestNetworkModule
import br.com.thiagozg.easysimulator.di.modules.TestRetrofitModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    TestAndroidModule::class,
    TestNetworkModule::class,
    TestRetrofitModule::class,
    BuildersModule::class]
)
interface TestAppComponent : AndroidInjector<TestApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<TestApplication>()

}
