package br.com.thiagozg.easysimulator.di.modules

import android.content.Context
import br.com.thiagozg.easysimulator.TestApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TestAndroidModule {

    private val PREFERENCES = "Preferences"

    @Provides
    @Singleton
    fun provideContext(application: TestApplication) = application.applicationContext

    @Provides
    @Singleton
    fun providesSharedPreferences(context: Context) = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)

}
