package br.com.thiagozg.easysimulator.features

import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before

open class BaseActivityTest {

    protected val server by lazy { MockWebServer() }

    @Before
    fun setUp() {
        server.start(36004)
        server.url("/")
    }

    @After
    fun tearDown() = server.shutdown()

}