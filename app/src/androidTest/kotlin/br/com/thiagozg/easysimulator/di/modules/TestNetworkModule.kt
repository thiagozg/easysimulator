package br.com.thiagozg.easysimulator.di.modules

import android.preference.PreferenceManager
import br.com.thiagozg.easysimulator.TestApplication
import br.com.thiagozg.easysimulator.model.EasyApi
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class TestNetworkModule {

    val TIMEOUT_SECONDS = 60

    @Provides
    @Singleton
    fun provideHttpCache(app: TestApplication): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(app.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideGson() = GsonBuilder().create()

    @Provides
    @Singleton
    fun providesOkHttpClient(cache: Cache): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .addNetworkInterceptor(logging)
                .addInterceptor(logging)
                .cache(cache)
                .build()
    }

    @Provides
    @Singleton
    fun providesEasyApi(retrofit: Retrofit) = retrofit.create(EasyApi::class.java)

}
