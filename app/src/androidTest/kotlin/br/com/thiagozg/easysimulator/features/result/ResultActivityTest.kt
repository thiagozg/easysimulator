package br.com.thiagozg.easysimulator.features.result

import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.runner.AndroidJUnit4
import br.com.thiagozg.easysimulator.features.BaseActivityTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ResultActivityTest : BaseActivityTest() {

    @get:Rule val activityRule = IntentsTestRule(ResultActivity::class.java, false, false)

    @Test
    fun shouldShowData() {
        arrange {
            mockSimulatorRequest(server, "query_result_success.json")
            startResultActivity(activityRule)
        }
        assert {
            checkDataIsShowing()
        }
    }

    private fun arrange(func: ResultActivityArrangeRobot.() -> Unit) = ResultActivityArrangeRobot().apply { func() }

    private fun assert(func: ResultActivityAssertRobot.() -> Unit) = ResultActivityAssertRobot().apply { func() }

}