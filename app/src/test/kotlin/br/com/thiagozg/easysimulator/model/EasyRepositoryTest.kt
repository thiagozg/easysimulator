package br.com.thiagozg.easysimulator.model

import br.com.thiagozg.easysimulator.model.domain.FormDTO
import br.com.thiagozg.easysimulator.model.domain.SimulatorResultVO
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Test

class EasyRepositoryTest {

    val repository = mock<EasyRepository>()
    val formDTO = FormDTO(investedAmount = "1000", rate = "123", maturityDate = "2023-10-10")

    @Test
    fun `test createSimulation - single should be completed`() {
        val simulatorResultVO = mock<SimulatorResultVO>()
        whenever(repository.createSimulation(formDTO))
            .thenReturn(Single.just(simulatorResultVO))

        repository.createSimulation(formDTO)
            .test()
            .assertResult(simulatorResultVO)
            .assertComplete()
            .isDisposed
    }

    @Test
    fun `test createSimulation - single should be error`() {
        val errorVO = mock<Exception>()
        whenever(repository.createSimulation(formDTO))
            .thenReturn(Single.error(errorVO))

        repository.createSimulation(formDTO)
            .test()
            .assertError(errorVO)
            .isTerminated
    }

}