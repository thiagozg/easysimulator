package br.com.thiagozg.easysimulator.features.result

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import br.com.thiagozg.easysimulator.model.EasyRepository
import br.com.thiagozg.easysimulator.model.domain.FormDTO
import br.com.thiagozg.easysimulator.model.domain.SimulatorResultVO
import br.com.thiagozg.easysimulator.model.domain.StateResponse
import com.nhaarman.mockito_kotlin.*
import io.github.plastix.rxschedulerrule.RxSchedulerRule
import org.junit.After
import org.junit.Rule
import org.junit.Test

class ResultViewModelTest {

    @get:Rule val archRule = InstantTaskExecutorRule()

    @get:Rule var schedulersRule = RxSchedulerRule()

    val easyRepository = mock<EasyRepository>()
    val viewModel by lazy { spy(ResultViewModel(easyRepository)) }

    val observerState = mock<Observer<StateResponse>>()

    @After
    fun tearDown() {
        reset(easyRepository, observerState)
    }

    @Test
    fun `when createSimulation is called - should return StateSuccess`() {
        val formDTO = mock<FormDTO>()
        val simulatorResultVO = mock<SimulatorResultVO>()

        arrange {
            mockSimulationRequestSuccess(easyRepository, formDTO, simulatorResultVO)
        }

        act {
            callCreateSimulation(formDTO, viewModel, observerState)
        }

        assert {
            checkSimulatorResultVO(simulatorResultVO, observerState)
        }
    }

    @Test
    fun `when createSimulation is called - should return StateError`() {
        val formDTO = mock<FormDTO>()
        val errorVO = mock<Exception>()

        arrange {
            mockSimulationRequestError(easyRepository, formDTO, errorVO)
        }

        act {
            callCreateSimulation(formDTO, viewModel, observerState)
        }

        assert {
            checkResultErrorVO(errorVO, observerState)
        }
    }

    private fun arrange(func: ResultViewModelArrangeRobot.() -> Unit) = ResultViewModelArrangeRobot().apply { func() }

    private fun act(func: ResultViewModelActRobot.() -> Unit) = ResultViewModelActRobot().apply { func() }

    private fun assert(func: ResultViewModelAssertRobot.() -> Unit) = ResultViewModelAssertRobot().apply { func() }

}