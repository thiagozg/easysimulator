package br.com.thiagozg.easysimulator.features.result

import android.arch.lifecycle.Observer
import br.com.thiagozg.easysimulator.model.EasyRepository
import br.com.thiagozg.easysimulator.model.domain.*
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.hamcrest.CoreMatchers
import org.junit.Assert.assertThat
import org.mockito.ArgumentCaptor

class ResultViewModelArrangeRobot {
    fun mockSimulationRequestSuccess(easyRepository: EasyRepository,
                                     formDTO: FormDTO,
                                     simulatorResultVO: SimulatorResultVO) {
        whenever(easyRepository.createSimulation(formDTO))
            .thenReturn(Single.just(simulatorResultVO))
    }

    fun mockSimulationRequestError(easyRepository: EasyRepository,
                                   formDTO: FormDTO,
                                   errorVO: Exception) {
        whenever(easyRepository.createSimulation(formDTO))
            .thenReturn(Single.error(errorVO))
    }
}

class ResultViewModelActRobot {
    fun callCreateSimulation(formDTO: FormDTO,
                             viewModel: ResultViewModel,
                             observerState: Observer<StateResponse>) {
        viewModel.getSimulation().observeForever(observerState)
        viewModel.createSimulation(formDTO)
        viewModel.getSimulation().removeObserver(observerState)
    }
}

class ResultViewModelAssertRobot {
    fun checkSimulatorResultVO(simulatorResultVO: SimulatorResultVO,
                               observerState: Observer<StateResponse>) {
        argumentCaptor {
            val expectedSuccessState = StateSuccess(simulatorResultVO)
            verify(observerState, times(1)).onChanged(capture())
            val (successState) = allValues
            val data = (successState as StateSuccess<*>).data
            assertThat(data, CoreMatchers.instanceOf(SimulatorResultVO::class.java))
            assertEquals(expectedSuccessState.data, data)
        }
    }

    fun checkResultErrorVO(errorVO: Exception, observerState: Observer<StateResponse>) {
        argumentCaptor {
            val expectedErrorState = StateError(errorVO)
            verify(observerState, times(1)).onChanged(capture())
            val (errorState) = allValues
            val error = (errorState as StateError).error
            assertThat(error, CoreMatchers.instanceOf(Exception::class.java))
            assertEquals(expectedErrorState.error, error)
        }
    }

    private fun argumentCaptor(func: ArgumentCaptor<StateResponse>.() -> Unit) =
        ArgumentCaptor.forClass(StateResponse::class.java).apply { func() }
}
