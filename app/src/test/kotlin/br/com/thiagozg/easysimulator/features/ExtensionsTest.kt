package br.com.thiagozg.easysimulator.features

import org.junit.Assert.*
import org.junit.Test

class ExtensionsTest {

    @Test
    fun testFormatToMoney() {
        assertEquals("R$ 0,00", "0".formatToMoney())
        assertEquals("-R$ 5,00", "-500".formatToMoney())
        assertEquals("R$ 0,12", "0.12".formatToMoney())
        assertEquals("R$ 2.000,00", "200000".formatToMoney())
    }

    @Test
    fun testFormatToPercent() {
        assertEquals("100.00%", "100".formatToPercent())
        assertEquals("123.00%", "123".formatToPercent())
        assertEquals("-99.00%", "-99".formatToPercent())
        assertEquals("0.00%", "0".formatToPercent())
        assertEquals("0.53%", "0.53".formatToPercent())
    }

    @Test
    fun testIsValidDate() {
        assertFalse("10/10/123456".isValidDate())
        assertFalse("10/00/2015".isValidDate())
        assertFalse("00/10/2015".isValidDate())
        assertFalse("32/10/2015".isValidDate())
        assertFalse("31/02/2015".isValidDate())
        assertTrue("15/10/2015".isValidDate())
        assertTrue("31/01/2015".isValidDate())
    }

    @Test
    fun testIsFutureDate() {
        assertFalse("15/10/2000".isFutureDate())
        assertTrue("15/10/3000".isFutureDate())
    }

}