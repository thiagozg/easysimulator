package br.com.thiagozg.easysimulator.di.modules

import br.com.thiagozg.easysimulator.CustomApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AndroidModule {

    @Provides
    @Singleton
    fun provideContext(application: CustomApplication) = application.applicationContext

}
