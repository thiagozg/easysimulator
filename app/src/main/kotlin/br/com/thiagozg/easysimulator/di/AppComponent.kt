package br.com.thiagozg.easysimulator.di

import br.com.thiagozg.easysimulator.CustomApplication
import br.com.thiagozg.easysimulator.di.modules.AndroidModule
import br.com.thiagozg.easysimulator.di.modules.BuildersModule
import br.com.thiagozg.easysimulator.di.modules.NetworkModule
import br.com.thiagozg.easysimulator.di.modules.RetrofitModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AndroidModule::class,
    NetworkModule::class,
    RetrofitModule::class,
    BuildersModule::class]
)
interface AppComponent : AndroidInjector<CustomApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<CustomApplication>()

}
