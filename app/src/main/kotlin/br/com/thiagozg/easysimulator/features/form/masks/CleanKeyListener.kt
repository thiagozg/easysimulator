package br.com.thiagozg.easysimulator.features.form.masks

import android.view.KeyEvent
import android.view.View
import android.widget.EditText

class CleanKeyListener(private val editText: EditText) : View.OnKeyListener {
    private var clicks = 0

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (clicks == 0) {
            editText.text.clear()
        }
        clicks++
        return false
    }

}
