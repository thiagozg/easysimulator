package br.com.thiagozg.easysimulator.model.domain

sealed class StateResponse
class StateSuccess<T>(val data: T) : StateResponse()
class StateError(val error: Throwable) : StateResponse()