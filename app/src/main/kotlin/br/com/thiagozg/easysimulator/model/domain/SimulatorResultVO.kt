package br.com.thiagozg.easysimulator.model.domain

import com.google.gson.annotations.SerializedName

data class SimulatorResultVO(
    @SerializedName("investmentParameter") val investmentParameterVO: InvestmentParameterVO,
    @SerializedName("grossAmount") val grossAmount: Double,
    @SerializedName("taxesAmount") val taxesAmount: Double,
    @SerializedName("netAmount") val netAmount: Double,
    @SerializedName("grossAmountProfit") val grossAmountProfit: Double,
    @SerializedName("netAmountProfit") val netAmountProfit: Double,
    @SerializedName("annualGrossRateProfit") val annualGrossRateProfit: Double,
    @SerializedName("monthlyGrossRateProfit") val monthlyGrossRateProfit: Double,
    @SerializedName("dailyGrossRateProfit") val dailyGrossRateProfit: Double,
    @SerializedName("taxesRate") val taxesRate: Double,
    @SerializedName("rateProfit") val rateProfit: Double,
    @SerializedName("annualNetRateProfit") val annualNetRateProfit: Double
)