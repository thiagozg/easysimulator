package br.com.thiagozg.easysimulator.features.form.masks

import android.widget.Button
import android.widget.EditText
import java.lang.ref.WeakReference


class DateMask(
    editText: EditText,
    button: Button,
    nextChain: ValidationFormInputChain?
) : ValidationFormInputChain(button, nextChain) {

    private val editTextWeakReference = WeakReference(editText)
    private var isUpdating: Boolean = false
    private var oldTxt = ""

    override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
        val editText = editTextWeakReference.get()!!

        val str = charSequence.replace("[.-/()]".toRegex(), "")
        var maskCurrent = ""

        if (isUpdating) {
            oldTxt = str
            isUpdating = false
            return
        }

        var i = 0
        for (m in MASK_DATE.toCharArray()) {
            if (m != '#' && str.length > oldTxt.length) {
                maskCurrent += m
                continue
            }
            try {
                maskCurrent += str[i]
            } catch (e: Exception) {
                break
            }

            i++
        }
        isUpdating = true
        editText.setText(maskCurrent)
        editText.setSelection(maskCurrent.length)

        super.onTextChanged(charSequence, start, before, count)
    }

    companion object {
        const val MASK_DATE = "##/##/####"
    }
}