package br.com.thiagozg.easysimulator.model

import br.com.thiagozg.easysimulator.model.domain.SimulatorResultVO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface EasyApi {

    @GET("calculator/simulate")
    fun simulate(
        @Query("investedAmount") investedAmount: Double,
        @Query("index") index: String,
        @Query("rate") rate: Double,
        @Query("isTaxFree") isTaxFree: Boolean,
        @Query("maturityDate") maturityDate: String): Single<SimulatorResultVO>

}