package br.com.thiagozg.easysimulator.features

import android.annotation.SuppressLint
import android.widget.TextView
import java.math.BigDecimal
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

fun TextView.textValue() = text.toString()

fun String.formatToMoney(): String {
    val cleanString = this.replace("[R$,. ]".toRegex(), "")
    val parsed = BigDecimal(cleanString)
        .setScale(2, BigDecimal.ROUND_FLOOR)
        .divide(BigDecimal(100), BigDecimal.ROUND_FLOOR)
    return NumberFormat.getCurrencyInstance().format(parsed)
}

fun String.formatToPercent(): String {
    val parsed = BigDecimal(this).setScale(2, BigDecimal.ROUND_FLOOR)
    return "$parsed%"
}

fun String.isValidDate(): Boolean {
    val datePattern = "(0?[1-9]|[12][0-9]|3[01])[/.-](0?[1-9]|1[012])[/.-]((19|20)\\d\\d)"
    val pattern: Pattern = Pattern.compile(datePattern)
    val matcher: Matcher = pattern.matcher(this)

    if (matcher.matches()) {
        matcher.reset()

        if (matcher.find()) {
            val day = matcher.group(1)
            val month = matcher.group(2)
            val year = Integer.parseInt(matcher.group(3))

            return if (day == "31" && (month == "4" || month == "6" || month == "9" ||
                    month == "11" || month == "04" || month == "06" ||
                    month == "09")) {
                false // only 1,3,5,7,8,10,12 has 31 days
            } else if (month == "2" || month == "02") {
                //leap year
                if (year % 4 == 0) {
                    !(day == "30" || day == "31")
                } else {
                    !(day == "29" || day == "30" || day == "31")
                }
            } else {
                true
            }
        } else {
            return false
        }
    } else {
        return false
    }
}

@SuppressLint("SimpleDateFormat")
fun String.isFutureDate(): Boolean {
    val dateFormat = SimpleDateFormat("dd/MM/yyyy")
    return dateFormat.parse(this).after(Date())
}