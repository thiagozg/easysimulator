package br.com.thiagozg.easysimulator.features.form.masks

import android.text.Editable
import android.text.TextWatcher
import android.widget.Button

open class ValidationFormInputChain(
    private val button: Button,
    private val nextChain: ValidationFormInputChain?
) : TextWatcher {

    private var shouldAllowButton: Boolean = false

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

    override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
        shouldAllowButton = charSequence.isNotEmpty()
    }

    override fun afterTextChanged(editable: Editable) {
        button.isEnabled = validateInput()
    }

    fun validateInput() : Boolean {
        if (nextChain != null) {
            return shouldAllowButton && nextChain.validateInput()
        }

        return shouldAllowButton
    }

}