package br.com.thiagozg.easysimulator.features.result

import br.com.thiagozg.easysimulator.model.EasyApi
import br.com.thiagozg.easysimulator.model.EasyRepository
import dagger.Module
import dagger.Provides

@Module
class ResultModule {

    @Provides
    fun providesResultViewModel(easyRepository: EasyRepository) = ResultViewModel(easyRepository)

    @Provides
    fun providesEasyRepository(easyApi: EasyApi) = EasyRepository(easyApi)

}