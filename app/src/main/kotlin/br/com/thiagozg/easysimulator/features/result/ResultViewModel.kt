package br.com.thiagozg.easysimulator.features.result

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import br.com.thiagozg.easysimulator.model.EasyRepository
import br.com.thiagozg.easysimulator.model.domain.FormDTO
import br.com.thiagozg.easysimulator.model.domain.StateError
import br.com.thiagozg.easysimulator.model.domain.StateResponse
import br.com.thiagozg.easysimulator.model.domain.StateSuccess
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ResultViewModel(private val easyRepository: EasyRepository) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val resultLiveData = MutableLiveData<StateResponse>()

    fun getSimulation() = resultLiveData

    fun createSimulation(formDTO: FormDTO) {
        disposables.add(
            easyRepository
                .createSimulation(formDTO)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe( {
                    resultLiveData.value = StateSuccess(it)
                }, {
                    resultLiveData.value = StateError(it)
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}