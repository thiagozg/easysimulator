package br.com.thiagozg.easysimulator.features.result

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import br.com.thiagozg.easysimulator.R
import br.com.thiagozg.easysimulator.features.formatToMoney
import br.com.thiagozg.easysimulator.features.formatToPercent
import br.com.thiagozg.easysimulator.model.domain.FormDTO
import br.com.thiagozg.easysimulator.model.domain.SimulatorResultVO
import br.com.thiagozg.easysimulator.model.domain.StateError
import br.com.thiagozg.easysimulator.model.domain.StateSuccess
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_result.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

class ResultActivity : AppCompatActivity() {

    @Inject lateinit var viewModel: ResultViewModel
    private lateinit var formDTO: FormDTO

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        observeSimulationResult()
        simulateInvestment()
        btSimulateAgain.setOnClickListener { onBackPressed() }
    }

    private fun simulateInvestment() {
        formDTO = intent.getParcelableExtra(KEY_INPUT_FORM)
        viewModel.createSimulation(formDTO)
    }

    private fun observeSimulationResult() {
        viewModel.getSimulation().observe(this, Observer { stateResponse ->
            stateResponse?.let {
                when(stateResponse) {
                    is StateSuccess<*> -> handleSuccessResult(stateResponse.data as? SimulatorResultVO)
                    is StateError -> handleErrorResult(stateResponse.error as? Throwable)
                }
            }
        })
    }

    private fun handleSuccessResult(data: SimulatorResultVO?) {
        data?.run {
            tvTotalValue.text = grossAmount.toString().formatToMoney()

            val grossAmountProfitApi = grossAmountProfit.toString().formatToMoney()
            val blueGrossAmountProfit = SpannableString("${labelTotalIncome.text} $grossAmountProfitApi").apply {
                setSpan(ForegroundColorSpan(
                    ContextCompat.getColor(
                        this@ResultActivity, R.color.colorPrimary)),
                    20, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
            labelTotalIncome.text = blueGrossAmountProfit

            tvValueInitial.text = formDTO.investedAmount
            tvValueRaw.text = grossAmount.toString().formatToMoney()
            tvIncomeValue.text = grossAmountProfitApi
            tvItInvestment.text = "${taxesAmount.toString().formatToMoney()} (${taxesRate.toString().formatToPercent()})"
            tvInvestmentNetValue.text = netAmount.toString().formatToMoney()

            tvRescueDate.text = formDTO.maturityDate.replace("-", "/")
            tvRunningDays.text = investmentParameterVO.maturityTotalDays.toString()
            tvMonthlyIncome.text = monthlyGrossRateProfit.toString().formatToPercent()
            tvCdiPercentage.text = formDTO.rate
            tvAnnualProfitability.text = annualGrossRateProfit.toString().formatToPercent()
            tvPeriodProfitability.text = rateProfit.toString().formatToPercent()
        } ?: handleErrorResult()
    }

    private fun handleErrorResult(error: Throwable? = null) {
        longToast(error?.message ?: getString(R.string.msg_generic_error))
        onBackPressed()
    }

    companion object {
        const val KEY_INPUT_FORM = "keyInputForm"
    }

}
