package br.com.thiagozg.easysimulator.model

import br.com.thiagozg.easysimulator.model.domain.FormDTO

class EasyRepository(private val easyApi: EasyApi) {

    fun createSimulation(formDTO: FormDTO) = formDTO.run {
        easyApi.simulate(
            investedAmount.cleanInvestedAmount(),
            index,
            rate.cleanCdiPercent(),
            isTaxFree,
            maturityDate.formatDateToApi())
    }

    private fun String.cleanInvestedAmount() = toString().replace("[R$ ]".toRegex(), "")
        .replace(".", "").replace(",", ".").toDouble()

    private fun String.cleanCdiPercent() = toString().replace("%", "").toDouble()

    private fun String.formatDateToApi() = "${substring(6, length)}-${substring(3, 5)}-${substring(0, 2)}"

}