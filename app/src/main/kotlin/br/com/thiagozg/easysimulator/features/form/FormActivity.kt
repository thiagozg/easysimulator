package br.com.thiagozg.easysimulator.features.form

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.thiagozg.easysimulator.R
import br.com.thiagozg.easysimulator.features.form.masks.CleanKeyListener
import br.com.thiagozg.easysimulator.features.form.masks.DateMask
import br.com.thiagozg.easysimulator.features.form.masks.MoneyMask
import br.com.thiagozg.easysimulator.features.form.masks.PercentMask
import br.com.thiagozg.easysimulator.features.isFutureDate
import br.com.thiagozg.easysimulator.features.result.ResultActivity
import br.com.thiagozg.easysimulator.features.result.ResultActivity.Companion.KEY_INPUT_FORM
import br.com.thiagozg.easysimulator.features.textValue
import br.com.thiagozg.easysimulator.features.isValidDate
import br.com.thiagozg.easysimulator.model.domain.FormDTO
import kotlinx.android.synthetic.main.activity_form.*
import org.jetbrains.anko.startActivity

class FormActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        initViews()
    }

    private fun initViews() {
        val percentChain = PercentMask(tiePercent, btSimulate, null)
        val dateChain = DateMask(tieDate, btSimulate, percentChain)
        val amountChain = MoneyMask(tieAmountValue, btSimulate, dateChain)

        tiePercent.run {
            setText("0")
            addTextChangedListener(percentChain)
            setOnKeyListener(CleanKeyListener(this))
        }
        tieDate.run {
            setText(getString(R.string.hint_format_date))
            addTextChangedListener(dateChain)
            setOnKeyListener(CleanKeyListener(this))
        }
        tieAmountValue.run {
            setText("0")
            addTextChangedListener(amountChain)
            setOnKeyListener(CleanKeyListener(this))
        }

        btSimulate.setOnClickListener { onSimulateClick() }
    }

    private fun onSimulateClick() {
        tieDate.textValue().run {
            if (isValidDate()) {
                if (isFutureDate()) {
                    val formDTO = FormDTO(
                        investedAmount = tieAmountValue.textValue(),
                        maturityDate = tieDate.textValue(),
                        rate = tiePercent.textValue()
                    )
                    startActivity<ResultActivity>(
                        KEY_INPUT_FORM to formDTO
                    )
                } else {
                    tieDate.error = getString(R.string.input_date_error_future)
                }
            } else {
                tieDate.error = getString(R.string.input_date_error)
            }
        }
    }

}