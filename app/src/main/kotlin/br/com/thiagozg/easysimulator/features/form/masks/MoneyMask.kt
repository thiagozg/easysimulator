package br.com.thiagozg.easysimulator.features.form.masks

import android.text.Editable
import android.widget.Button
import android.widget.EditText
import br.com.thiagozg.easysimulator.features.formatToMoney
import java.lang.ref.WeakReference


class MoneyMask(
    editText: EditText,
    button: Button,
    nextChain: ValidationFormInputChain?
) : ValidationFormInputChain(button, nextChain) {

    private val editTextWeakReference = WeakReference(editText)

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get()!!

        if (editable.toString().isNotEmpty()) {
            editText.removeTextChangedListener(this)
            val formatted = editable.toString().formatToMoney()
            editText.setText(formatted)
            editText.setSelection(formatted.length)
            editText.addTextChangedListener(this)
        }

        super.afterTextChanged(editable)
    }
}