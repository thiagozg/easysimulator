package br.com.thiagozg.easysimulator.features.form.masks

import android.text.Editable
import android.widget.Button
import android.widget.EditText
import java.lang.ref.WeakReference


class PercentMask(
    editText: EditText,
    button: Button,
    nextChain: ValidationFormInputChain?
) : ValidationFormInputChain(button, nextChain) {

    private val editTextWeakReference = WeakReference(editText)

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get()!!
        val value = editable.toString()

        if (value.isNotEmpty()) {
            editText.removeTextChangedListener(this)
            val formatted: String

            if (value != "%") {
                val cleanString = value.replace("[%]".toRegex(), "")
                formatted = "$cleanString%"
            } else {
                formatted = "0%"
            }

            editText.setText(formatted)
            editText.setSelection(formatted.length - 1)

            editText.addTextChangedListener(this)
        }

        super.afterTextChanged(editable)
    }
}