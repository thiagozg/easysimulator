package br.com.thiagozg.easysimulator.di.modules

import br.com.thiagozg.easysimulator.features.result.ResultActivity
import br.com.thiagozg.easysimulator.features.result.ResultModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector(modules = [ResultModule::class])
    internal abstract fun bindResultFeature(): ResultActivity
}
